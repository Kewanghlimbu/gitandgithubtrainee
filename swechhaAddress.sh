#!/bin/sh

display_menu() {
    echo "Address Book Menu:"
    echo "1. Search"
    echo "2. Add Entry"
    echo "3. Remove / Edit Entry"
    echo "4. Display All Entries"
    echo "5. Quit"
}

search_entries() {
    echo "Enter search term:"
    read search_term
    grep -i "$search_term" address_book.txt
}

add_entry() {
    echo "Enter Name:"
    read name
    echo "Enter Surname:"
    read surname
    echo "Enter Email:"
    read email
    echo "Enter Phone:"
    read phone

    # Check if the entry already exists, q for executing grep quietly without displaying output
    if grep -q "$name $surname" address_book.txt; then
        echo "This entry already exists. Do you want to edit it? (y/n)"
        read choice
        if [ "$choice" = "y" ]; then
            edit_entry "$name $surname"
            return
        else
            echo "Entry not added."
            return
        fi
    fi

    #saving the entry to the file
    echo "$name $surname | Email: $email | Phone: $phone" >> address_book.txt
    echo "Data added successfully."
}

remove_edit_entry() {
    echo "Enter search term:"
    read search_term
    matching_entries=$(grep -i "$search_term" address_book.txt)
    echo "$matching_entries"
    # z indicates if zero number of matching entries
    if [ -z "$matching_entries" ]; then
        echo "No matching entries found."
        return
    fi

    echo "Enter the line number to remove/edit:"
    read line_number
    selected_entry=$(echo "$matching_entries" | sed -n "${line_number}p")

    if [ -z "$selected_entry" ]; then
        echo "Invalid line number."
        return
    fi

    echo "Selected Entry:"
    echo "$selected_entry"

    echo "Do you want to (1) remove or (2) edit this entry?"
    read choice
    case $choice in
        1)
            remove_entry "$selected_entry"
            ;;
        2)
            edit_entry "$selected_entry"
            ;;
        *)
            echo "Invalid choice."
            ;;
    esac
}

# Function to remove an entry
remove_entry() {
    entry_to_remove="$1"
    sed -i "/$entry_to_remove/d" address_book.txt
    echo "Entry removed successfully."
}

# Function to edit an entry
edit_entry() {
    entry_to_edit="$1"
    echo "Editing Entry: $entry_to_edit"
   
    echo "Name [ $name ]"
    read new_name
    name=${new_name:-$name}

    echo "Surname [ $surname ]"
    read new_surname
    surname=${new_surname:-$surname}

    echo "Email [ $email ]"
    read new_email
    email=${new_email:-$email}

    echo "Phone [ $phone ]"
    read new_phone
    phone=${new_phone:-$phone}

    # Remove the old entry
    remove_entry "$entry_to_edit"

    # Add the updated entry
    echo "$name $surname | Email: $email | Phone: $phone" >> address_book.txt
    echo "Entry edited successfully."
}

# Main program loop
while true; do
    display_menu
    echo "Enter your choice:"
    read choice

    case $choice in
        1)
            search_entries
            ;;
        2)
            add_entry
            ;;
        3)
            remove_edit_entry
            ;;
        4)
            cat address_book.txt
            ;;
        5)
            echo "Goodbye!"
            exit 0
            ;;
        *)
            echo "Invalid choice."
            ;;
    esac
done
